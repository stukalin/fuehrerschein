﻿$(function () {
    $('#ergebnisliste .head').next().hide();
    $('#ergebnisliste .head.active').next().show();

    $('#ergebnisliste .head a').click(function () {
        if ($(this.parentNode).next().is(':visible')) {
            $(this.parentNode).removeClass('active');
        } else {
            $(this.parentNode).addClass('active');
        }
        $(this.parentNode).next().slideToggle('normal');
        return false;
    });

    if ($(window).outerWidth() > 745) {
        $('a.jLightBox').lightBox();
    } else {
        $('a.jLightBox').click(function (e) {
            e.preventDefault();
        });

        if ($('#tblPruefungSummary').length > 0) {

            var respTable = '';
            var respTblHead = [];

            $('#tblPruefungSummary thead th').each(function () {
                respTblHead.push($.trim($(this).html()));
            });
            $('#tblPruefungSummary tbody tr').each(function () {
                var respTblTd = [];
                $(this).find('td').each(function () {
                    respTblTd.push($.trim($(this).html()));
                });

                respTable += '<div class="respTableBox">';
                respTable += '<div class="respTableBoxHead">' + respTblHead[0] + ' ' + respTblTd[0] + ': ' + respTblTd[6] + '</div>';
                respTable += '<div class="respTableBoxBody">';
                respTable += '<b>' + respTblHead[1] + '</b><br />';
                respTable += respTblHead[4] + ':<b> ' + respTblTd[1] + '</b><br />';
                respTable += respTblHead[5] + ':<b> ' + respTblTd[2] + '</b><br />';
                respTable += respTblHead[6] + ':<b> ' + respTblTd[3] + '</b><br />';
                respTable += respTblHead[7] + ':<b> ' + respTblTd[4] + '</b><br />';
                respTable += respTblHead[2] + ':<b> ' + respTblTd[5] + '</b>';
                respTable += '</div>';
                respTable += '</div>';
            });

            $('#tblPruefungSummary').replaceWith(respTable);

        }
    }
});

